<?php
/**
 * Testing out the Tag Parser
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
use Exo\Tag\Parser;
include('classes/Parser.php');
$input = file_get_contents('input.html');
$parser = new Parser($input);
?>

<?php $tags = $parser->parse(); ?>
<?php foreach ($tags as $tag): ?>
<?= $tag->_string ?>

<?php endforeach; ?>

<?php $tags = $parser->parse('<test id="123" />'); ?>
<?php foreach ($tags as $tag): ?>
<?= $tag->_string ?>

<?php endforeach; ?>

